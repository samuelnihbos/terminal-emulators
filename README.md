<h2 align="center">Everblush Terminal Config's</h2>

<p>
<h4 align="center"> <i>Everblush for all different terminal emulators</i> </h4>
</p> 
<p align="center"> 
<img src="https://img.shields.io/github/stars/Everblush/terminal-emulators?color=e5c76b&labelColor=22292b&style=for-the-badge"> <img src="https://img.shields.io/github/issues/Everblush/terminal-emulators?color=67b0e8&labelColor=22292b&style=for-the-badge">
<img src="https://img.shields.io/static/v1?label=license&message=MIT&color=8ccf7e&labelColor=22292b&style=for-the-badge">
<img src="https://img.shields.io/github/forks/Everblush/terminal-emulators?color=e74c4c&labelColor=1b2224&style=for-the-badge">
</p>

## Installation 

- [Alacritty](https://github.com/Everblush/terminal-emulators/tree/main/src/alacritty.yml) - wget src/alacritty.yml to your `~/.config/alacritty/alacritty.yml`. 
- [kitty](https://github.com/Everblush/terminal-emulators/tree/main/src/kitty.conf) - wget src/kitty.conf to your `~/.config/kitty/kitty.conf`.
- [st](https://github.com/Everblush/terminal-emulators/tree/main/st) - `sudo make install` into the cloned `src/st` directory.
- [xfce4-terminal](https://github.com/Everblush/terminal-emulators/tree/main/src/xfce4-terminal/terminalrc) - wget src/xfce4-terminal/terminalrc to your `~/.config/xfce4/terminal/terminalrc` 
- [gnome-terminal](https://github.com/Everblush/terminal-emulators/tree/main/src/gnome-terminal/Everblush.dconf) - visit src/gnome-terminal to get detailed instructions!

## Credits 💝
- [samuel](https://github.com/samuelnihbos)
